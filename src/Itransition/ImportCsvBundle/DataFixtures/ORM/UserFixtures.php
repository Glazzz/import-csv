<?php
namespace Itransition\ImportCsvBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Itransition\ImportCsvBundle\Entity\User;

class UserFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        # Create admin user
        $user1 = new User();
        $user1->setUsername( 'admin' );
        $user1->setPlainPassword( 'admin' );
        $user1->setEmail( 'admin@admin.com' );
        $user1->setEnabled( true );
        $user1->setSuperAdmin( true );
        $manager->persist( $user1 );

        # Create simple user
        $user2 = new User();
        $user2->setUsername( 'user' );
        $user2->setPlainPassword( 'user' );
        $user2->setEmail( 'user@user.com' );
        $user2->setEnabled( true );
        $user2->setSuperAdmin( false );
        $manager->persist( $user2 );

        # Add to database
        $manager->flush();
    }
}
