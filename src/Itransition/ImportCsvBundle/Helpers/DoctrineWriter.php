<?php
namespace Itransition\ImportCsvBundle\Helpers;

use Ddeboer\DataImport\Writer\DoctrineWriter as Ddeboer_DoctrineWriter;

class DoctrineWriter extends Ddeboer_DoctrineWriter
{
    /**
     * Number of entities to be persisted per flush
     * @var int
     */
    protected $batchSize = 20;

    /**
     * Counter for internal use
     * @var int
     */
    protected $counter = 0;

    /**
     * Get number of entities that may be persisted before a new flush
     * @return int
     */
    public function getBatchSize()
    {
        return $this->batchSize;
    }
    /**
     * Set number of entities that may be persisted before a new flush
     * @param  int $batchSize
     * @return DoctrineWriter
     */
    public function setBatchSize($batchSize)
    {
        $this->batchSize = $batchSize;

        return $this;
    }

    /**
     * @param array $item
     * @return $this
     */
    public function writeItem(array $item)
    {
        $this->counter++;

        $entity = $this->findOrCreateItem($item);

        $this->loadAssociationObjectsToEntity($item, $entity);
        $this->updateEntity($item, $entity);

        $this->entityManager->persist($entity);

        if (($this->counter % $this->batchSize) == 0) {

            $this->flushAndClear();
        }
        return $this;
    }

    protected function flushAndClear()
    {
        $this->entityManager->flush();
        $this->entityManager->clear($this->entityName);
    }
}