<?php
namespace Itransition\ImportCsvBundle\Helpers;

use Itransition\ImportCsvBundle\Entity\Product as Product_Entity;
use Doctrine\ORM\EntityManager;

class Product
{
    protected $manager;

    protected $lastErrors = [];

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Alias fields
     * @var array
     */
    protected $fieldsAliases = [
        'code'          => 'Product Code',
        'name'          => 'Product Name',
        'description'   => 'Product Description',
        'stock'         => 'Stock',
        'cost'          => 'Cost in GBP',
        'discontinued'  => 'Discontinued'
    ];

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getFieldsAlias()
    {
        $this->checkRequiredProperty('fieldsAliases');

        return $this->fieldsAliases;
    }

    /**
     * @param $fieldAlias
     * @return mixed
     * @throws \Exception
     */
    public function getFieldNameByAlias($fieldAlias)
    {
        $this->checkRequiredProperty('fieldsAliases');

        return array_search($fieldAlias, $this->fieldsAliases);
    }

    /**
     * @param $fieldName
     * @return bool
     * @throws \Exception
     */
    public function getFieldAliasByName($fieldName)
    {
        $this->checkRequiredProperty('fieldsAliases');

        if ( isset( $this->fieldsAliases[ $fieldName ] ) ) {
            return $this->fieldsAliases[ $fieldName ];
        }

        return false;
    }

    /**
     * @param array $aliasesList
     * @return array
     * @throws \Exception
     */
    public function getFieldsListByAliases(array $aliasesList)
    {
        $this->checkRequiredProperty('fieldsAliases');

        return array_replace( $aliasesList, array_keys( $this->fieldsAliases ) );
    }

    /**
     * @param $property
     * @return bool
     * @throws \Exception
     */
    private function checkRequiredProperty($property)
    {
        if ( !isset( $this->{ $property } ) ) {
            throw new \Exception( sprintf('Property "%s" is not undefined', $property) );
        }

        return true;
    }

    /**
     * Get last errors
     * @return array
     */
    public function getLastErrors()
    {
        return $this->lastErrors;
    }

    /**
     * Fill product and check valid data
     * @param Product_Entity $product
     * @param array $productData
     * @return bool|Product_Entity
     */
    public function fillProductData(Product_Entity $product, array $productData)
    {
        global $kernel;

        $product->setCode( $productData['code'] );
        $product->setName( $productData['name'] );
        $product->setDescription( $productData['description'] );
        $product->setStock( $productData['stock'] );
        $product->setCost( $productData['cost'] );

        $oldDiscontinued = $product->getDiscontinued();
        if ( $oldDiscontinued == null && $productData['discontinued'] != $oldDiscontinued ) {
            $product->setDiscontinued( new \DateTime() );
        }

        $validator = $kernel->getContainer()->get('validator');

        # Check is valid product
        $errors = $validator->validate( $product );

        # If is not valid
        if ( $errors->count() ) {
            $this->lastErrors = $errors[0]->getMessage();
            return false;
        }

        return $product;
    }

    /**
     * Update product by data
     * @param array $productData
     * @return bool
     */
    public function updateProduct(array $productData)
    {
        # Get product by id
        $product = $this->manager->getRepository('ItransitionImportCsvBundle:Product')->find( $productData['id'] );

        # if product not exists
        if ( !$product ) {
            return false;
        }

        # Fill product data and check validate
        $product = $this->fillProductData( $product, $productData );
        
        # If invalid product data
        if ( ! $product ) {
            return false;
        }

        # Update
        $this->manager->persist( $product );
        $this->manager->flush();

        return true;
    }

    /**
     * Create new product by data array
     * @param array $productData
     * @return bool|int false or product id
     */
    public function createProduct(array $productData)
    {
        $this->lastErrors = null;
        
        $product = new Product_Entity();
        
        # Fill product data and check validate
        $product = $this->fillProductData( $product, $productData );
        
        # If invalid product data
        if ( ! $product ) {
            return false;
        }
        
        # Create
        $this->manager->persist( $product );
        $this->manager->flush();
        
        return $product->getId();
    }
}