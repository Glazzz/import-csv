'use strict';

app.filter('dateString', function ( $filter ) {
    return function (input, format, timezone) {
        return (!!input) ? $filter('date')( Date.parse(input), format, timezone ) : '';
    };
});