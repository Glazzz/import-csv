'use strict';

var app = angular.module('testApp', ['ngRoute', 'ngResource', 'ngFileUpload']);

// Routers settings
app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl : '/api/v1/pages',
            controller  : 'IndexPageController'
        })
        .when('/add', {
            templateUrl : '/api/v1/pages/add',
            controller  : 'AddPageController'
        })
        .when('/import-csv', {
            templateUrl : '/api/v1/pages/import',
            controller  : 'ImportPageController'
        })
        .when('/records/:id', {
            templateUrl : '/api/v1/pages/record',
            controller  : 'RecordPageController'
        })
        .when('/logout', {
            controller  : 'LogoutController',
            template    : ''
        })
        .when('/login', {
            controller  : 'LoginController',
            template    : ''
        })
        .otherwise({
            templateUrl : '/api/v1/pages/404'
        });

    // Enable html5 links
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
});

// Set interpolate symbols (for remove twig conflicts)
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});