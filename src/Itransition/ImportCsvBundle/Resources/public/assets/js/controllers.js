'use strict';

app.controller('MainController', function($scope) {});

app.controller('AddPageController', function($scope) {});

app.controller('ImportPageController', function($scope) {});

app.controller('MenuController', function($scope, $location) {
    /* Activate menu active class */
    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
});

app.controller('IndexPageController', function($scope, Record, Security) {

    $scope.sortField    = 'id';
    $scope.reverse      = false;
    
    /* Check login and get all records */
    Security.checkLogin(function(response) {
        if ( response.success ) {

            /* Get all records */
            Record.query(function(response) {
                $scope.records = [];

                if ( response.success ) {
                    $scope.records = response.records;
                }
            });
        }
    });

    /* Sorting data */
    $scope.sort = function(fieldName) {
        if ( $scope.sortField === fieldName ) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };

    /* Find record by id and return record index */
    $scope.getIndexById = function(id) {
        for ( var i = 0; i < $scope.records.length; i++ ) {
            if ( id == $scope.records[ i ].id ) {
                return i;
            }
        }
        return null;
    };

    /* Delete record */
    $scope.deleteRecord = function (id) {
        var recordToDelete = $scope.records[ $scope.getIndexById( id ) ];

        if ( confirm('Are you sure you want to delete the record with ID: '+ recordToDelete.id +'?') ) {
            /* Check login and delete record */
            Security.checkLogin(function(response) {
                if ( response.success ) {
                    /* Delete record by id */
                    Record.delete( {id: recordToDelete.id}, function(response) {
                        if ( response.success ) {
                            $scope.records.splice( $scope.getIndexById( id ), 1 );
                        } else {
                            /* Error */
                            alert( response.message );
                        }
                    } );
                }
            });
        }
    }
});

app.controller('AddRecordController', function($scope, Record, Security, $timeout, $location) {
    /* Add new record */
    $scope.AddRecord = function(record) {
        $scope.response = null;
        record.discontinued = record.discontinued ? 'yes' : null;

        /* Check login and add record */
        Security.checkLogin(function(response) {

            if ( response.success ) {
                /* Add form token */
                record._token = document.getElementById('_token').value;
                
                Record.save( record, function(response) {
                    $scope.response = response;
                    record.discontinued = record.discontinued == 'yes';

                    if ( response.success ) {
                        /* Redirect to edit page */
                        $timeout(function() {
                            $location.url( '/records/' + response.id );
                        }, 2000);
                    }
                } );
            } else {
                $scope.response = response;
            }
        });
    }
});

app.controller('RecordPageController', function($scope, $rootScope, $routeParams) {
    /* Current record edit id */
    $rootScope.recordId = $routeParams.id;
});

app.controller('EditRecordController', function($scope, $rootScope, Record, Security, $location) {
    /* Check login and get record */
    Security.checkLogin(function(response) {

        if ( response.success ) {
            $scope.response = null;
            /* Get record by id */
            Record.get({id: $rootScope.recordId}, function(response) {
                /* Check valid response */
                if ( !response.success ) {
                    $location.url('/');
                    return false;
                }

                /* Format discontinued */
                if ( response.record.discontinued != null ) {
                    response.record.discontinued = true;
                }
                $scope.record = response.record;
            });
        } else {
            $scope.response = response;
        }
    });

    /* Edit current record */
    $scope.EditRecord = function() {
        $scope.response = null;

        /* Add form token */
        //$scope.record._token = document.getElementById('_token').value;

        /* Check login and update */
        Security.checkLogin(function(response) {
            if ( response.success ) {
                /* Add form token */
                $scope.record._token = document.getElementById('_token').value;
                
                Record.update( $scope.record , function (response) {
                    $scope.response = response;
                });
            } else {
                $scope.response = response;
            }
        });
    }
});

app.controller('ImportFileController', function($scope, $timeout, Security, Upload) {

    /* Import records from .csv file */
    $scope.importProcess = function(file, truncate) {

        Security.checkLogin(function (response) {
            if ( response.success ) {
                $scope.response = null;
                file.progress = 0;

                /* Add form token */
                var _token = document.getElementById('_token').value;

                /* Init upload */
                file.upload = Upload.upload({
                    url: '/api/v1/records/imports',
                    data: {
                        truncate: truncate ? 1 : 0,
                        file: file,
                        _token: _token
                    }
                });

                /* Upload request */
                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;

                        $scope.response = response.data;
                    });
                }, function (response) {
                    if (response.status > 0) {
                        $scope.errorMsg = response.status + ': ' + response.data;
                    }
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        });
    }
});

app.controller('LogoutController', function($scope, $http) {
    $http.get('/api/v1/security/logout').success( function(response) {
        if ( response.success ) {
            window.location.href = '/';
        }
    });
});

app.controller('LoginController', function($scope) {
    window.location.href = '/login';
});