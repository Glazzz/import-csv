'use strict';

app.factory("Record", function($resource) {

    return $resource("/api/v1/records/:id", {}, {
        query:   {method:"GET",     isArray:false},
        delete:  {method:"DELETE",  isArray:false},
        update:  {method:"PUT",     isArray:false}
    });
});

app.factory("Security", function($http) {

    var checkLogin = function(callback) {
        $http({
            url: '/api/v1/security/check/login',
            method: 'GET'
        }).then(function(response) {
            if ( typeof callback == 'function' ) {
                callback( response.data );
            }
        });
    };

    return {
        checkLogin: checkLogin
    }
});