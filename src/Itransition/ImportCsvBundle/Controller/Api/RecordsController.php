<?php
namespace Itransition\ImportCsvBundle\Controller\Api;

use Itransition\ImportCsvBundle\Entity\Product;
use Itransition\ImportCsvBundle\Entity\Upload;
use Itransition\ImportCsvBundle\Form\ProductType;
use Itransition\ImportCsvBundle\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Itransition\ImportCsvBundle\Helpers\Product as ProductHelper;
use Ddeboer\DataImport\Reader\CsvReader;

class RecordsController extends Controller
{
    /**
     * Get all records
     * Page: /api/v1/records
     * Method: GET
     * @return JsonResponse
     */
    public function getRecordsAction()
    {
        # Default response
        $response = [ 'success' => false, 'message' => 'Records not found' ];
        
        # Entity Manager
        $em = $this->getDoctrine()->getManager();

        # Get records
        $records = $em->getRepository('ItransitionImportCsvBundle:Product')->getRecords();
        
        # Generate response
        if ( $records ) {
            $response = [
                'success' => true,
                'records' => $records
            ];
        }

        return new JsonResponse( $response );
    }

    /**
     * Update record
     * Page: /api/v1/records
     * Method: PUT
     * @return JsonResponse
     */
    public function putRecordsAction()
    {
        # Default response
        $response = [ 'success' => false, 'message' => 'Invalid input' ];

        # Entity Manager
        $em = $this->getDoctrine()->getManager();
        
        # Init request
        $request  = Request::createFromGlobals();

        # Get data
        $data = json_decode( $request->getContent(), true );

        # Check id 
        if ( !isset( $data['id'] ) ) {
            return new JsonResponse( $response );
        }

        # Form
        $product = $em->getRepository('ItransitionImportCsvBundle:Product')->find( $data['id'] );

        # Change discontinued for form validation
        if ( $oldDiscontinued = $product->getDiscontinued() ) {
            $product->setDiscontinued( true );
        }

        $form = $this->createForm(ProductType::class, $product, ['method' => 'PUT', 'allow_extra_fields' => true]);
        $request->request->replace( $data );


        # Check form request
        $form->handleRequest( $request );

        # Check valid form
        if ( !$form->isValid() ) {

            # Error to message
            if ( count( $form->getErrors( true ) ) ) {
                $response['message'] = $form->getErrors()[0]->getMessage();
            }

            return new JsonResponse( $response );
        }

        # Set actual discontinued
        $product->setDiscontinued( $oldDiscontinued );
        if ( !$oldDiscontinued && $product->getDiscontinued() ) {
            $product->setDiscontinued( new \DateTime() );
        }

        # Update product
        $productHelper = new ProductHelper( $em );
        $result = $productHelper->updateProduct( $data );

        $response = [
            'success' => false,
            'message' => 'Update failed'
        ];

        if ( $result ) {
            $response = [
                'success' => true,
                'message' => 'Successfully updated'
            ];
        }

        return new JsonResponse( $response );
    }

    /**
     * Delete record
     * Page: /api/v1/records/:id
     * Method: Delete
     * @param $id
     * @return JsonResponse
     */
    public function deleteRecordsAction($id)
    {
        # Entity Manager
        $em = $this->getDoctrine()->getManager();

        # Get record by id
        $record = $em->getRepository('ItransitionImportCsvBundle:Product')->find( $id );

        # If record not found
        if ( ! $record ) {
            return new JsonResponse( [
                'success' => false,
                'message' => 'Invalid input'
            ] );
        }

        # Delete record
        $em->remove( $record );
        $em->flush();

        return new JsonResponse( [
            'success' => true,
            'message' => 'Successfully deleted'
        ] );
    }

    /**
     * Create new record
     * Page: /api/v1/records
     * Method: POST
     * @return Response
     */
    public function postRecordsAction(Request $request)
    {
        # Default response
        $response = ['success' => false, 'message' => 'Invalid Input'];

        # Entity Manager
        $em = $this->getDoctrine()->getManager();

        # Get data
        $data = json_decode( $request->getContent(), true );
        $request->request->replace($data);

        # Form
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        
        # Check form request
        $form->handleRequest( $request );

        # Check valid form
        if ( !$form->isValid() ) {

            # Error to message
            if ( count( $form->getErrors() ) ) {
                $response['message'] = $form->getErrors()[0]->getMessage();
            }

            return new JsonResponse( $response );
        }

        # Create product
        $productHelper = new ProductHelper( $em );
        $result = $productHelper->createProduct( $data );

        if ( !$result ) {
            # Get last errors (example: validation errors)
            $message = $productHelper->getLastErrors();

            $response = [
                'success' => false,
                'message' => $message
            ];
        } else {
            $response = [
                'success'   => true,
                'message'   => 'Successfully created',
                'id'        => $result
            ];
        }

        return new JsonResponse( $response );
    }

    /**
     * Get one record
     * Page: /api/v1/record/:id
     * Method: GET
     * @param $id
     * @return JsonResponse
     */
    public function getRecordAction($id)
    {
        # Default response
        $response = [ 'success' => false, 'message' => 'Record not found' ];
        
        # Entity manager
        $em = $this->getDoctrine()->getManager();

        # Get record
        $record = (array) $em->getRepository('ItransitionImportCsvBundle:Product')->getRecord( $id );
        
        # Generate response
        if ( $record ) {
            $response = [
                'success'   => true,
                'record'    => $record
            ];
        }

        return new JsonResponse( $response );
    }

    /**
     * Import products from .csv file
     * Page: /api/v1/records/imports
     * Method: POST
     * @return JsonResponse
     * @throws \Exception
     */
    public function postRecordsImportAction()
    {
        # Default response
        $response = [
            'success' => false,
            'message' => 'Invalid input'
        ];
        
        # Init request
        $request        = Request::createFromGlobals();

        $file           = $request->files->get('file');
        $shouldTruncate = (bool) $request->request->get('truncate');

        if ( !$file ) {
            return new JsonResponse( $response );
        }

        # Form
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        # Check form request
        $form->handleRequest( $request );

        # Check valid form
        if ( !$form->isValid() ) {

            # Error to message
            if ( count( $form->getErrors() ) ) {
                $response['message'] = $form->getErrors()[0]->getMessage();
            }

            return new JsonResponse( $response );
        }

        # Open file
        $csvFile    = new \SplFileObject( $file );

        # Csv read
        $csvReader  = new CsvReader( $csvFile );

        # Import products
        $importHandler = $this->container->get('itransition.import');
        $importHandler->run( $csvReader, false, $shouldTruncate );

        if ( ! $importHandler->getSuccessfulCount() ) {
            
            $response = [
                'success'   => false,
                'message'   => 'Import error',
                'errorLog'  => $importHandler->getErrorLog()
            ];
        } else {
            $response = [
                'success'           => true,
                'message'           => 'Import successfully',
                'total'             => ( $importHandler->getSuccessfulCount() + $importHandler->getSkippedCount() ),
                'successfulCount'   => $importHandler->getSuccessfulCount(),
                'skippedCount'      => $importHandler->getSkippedCount(),
                'errorLog'          => $importHandler->getErrorLog()
            ];
        }

        return new JsonResponse( $response );
    }
}