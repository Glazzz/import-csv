<?php
namespace Itransition\ImportCsvBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Itransition\ImportCsvBundle\Entity\Product;
use Itransition\ImportCsvBundle\Entity\Upload;
use Itransition\ImportCsvBundle\Form\ProductType;
use Itransition\ImportCsvBundle\Form\UploadType;

class PageController extends Controller
{
    /**
     * @Route("/api/v1/pages/add", name="_api_add")
     * @Method("GET")
     */
    public function getPagesAddAction()
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        return $this->render('ItransitionImportCsvBundle:Api/Pages:add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/api/v1/pages/record", name="_api_record")
     * @Method("GET")
     */
    public function getPagesEditAction()
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        return $this->render('ItransitionImportCsvBundle:Api/Pages:record.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/api/v1/pages/import", name="_api_import")
     * @Method("GET")
     */
    public function getPagesImportAction()
    {
        $upload = new Upload();

        $form = $this->createForm(UploadType::class, $upload);

        return $this->render('ItransitionImportCsvBundle:Api/Pages:import.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Api Page
     * @Route("/api/v1/pages/{slug}", defaults= {"slug" = "/"}, name="_api")
     * @Method("GET")
     */
    public function apiAction($slug)
    {
        $slugsView = [
            '/' => 'ItransitionImportCsvBundle:Api/Pages:index.html.twig',
            'record' => 'ItransitionImportCsvBundle:Api/Pages:record.html.twig',
            '404' => 'ItransitionImportCsvBundle:Api/Pages:404.html.twig',
        ];

        if ( !isset( $slugsView[ $slug ] ) ) {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render( $slugsView[ $slug ] );
    }
}