<?php
namespace Itransition\ImportCsvBundle\Controller\Api;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Logout;

class SecurityController extends FOSRestController
{
    /**
     * Check user login
     * @return JsonResponse
     */
    public function getCheckLoginAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');

        # if logged
        $result = $securityContext->isGranted('IS_AUTHENTICATED_FULLY');

        # Set response message
        $message = $result ? 'You are logged in' : 'You are not logged in';

        return new JsonResponse( [
            'success' => $result,
            'message' => $message
        ] );
    }


    /**
     * User logout
     * @return JsonResponse
     */
    public function getLogoutAction()
    {
        $this->get('security.token_storage')->setToken( null );

        return new JsonResponse( [
            'success' => true
        ] );
    }
}