<?php
namespace Itransition\ImportCsvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{
    /**
     * Main Page
     * @Route("{slug}/{child}", defaults={"slug" = "/", "child" = ""}, name="_main", requirements={
     *     "slug": "/|add|import-csv|records"
     *     })
     * @Method("GET")
     */
    public function mainAction($slug)
    {
        return $this->render( 'ItransitionImportCsvBundle::layout.html.twig' );
    }
}