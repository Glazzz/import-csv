<?php

namespace Itransition\ImportCsvBundle\Entity\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

/**
 * ProductRepository
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get one record by id
     * @param $id
     * @return array|null
     */
    public function getRecord($id)
    {
        $qb = $this->createQueryBuilder('r')->select('r');

        try {
            $record = $qb->where('r = :id')->setParameter('id', $id)
                ->getQuery()
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch ( NoResultException $e) {
            return array();
        }

        return $record;
    }
    /**
     * Get all records
     * @return array
     */
    public function getRecords()
    {
        $qb = $this->createQueryBuilder('r')->select('r');
        
        $data = $qb->getQuery()->getArrayResult();

        return $data;
    }
}
