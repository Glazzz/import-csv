<?php
namespace Itransition\ImportCsvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

class Upload
{
    /**
     * @Assert\NotBlank(message="Please, upload correct CSV file.")
     * @Assert\File(mimeTypes={ "text/plain" })
     */
    protected $file;

    /**
     * @Assert\Type("bool")
     */
    protected $truncate;

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;
        
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTruncate()
    {
        return $this->truncate;
    }

    /**
     * @param $truncate
     * @return $this
     */
    public function setTruncate($truncate)
    {
        $this->truncate = $truncate;
        
        return $this;
    }
}