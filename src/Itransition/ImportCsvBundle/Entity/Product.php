<?php
namespace Itransition\ImportCsvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use DateTime;

use Itransition\ImportCsvBundle\Validator\Constraints as ImportAssert;

/**
 * @ORM\Entity(repositoryClass="Itransition\ImportCsvBundle\Entity\Repository\ProductRepository")
 * @ORM\Table(name="tblProductData")
 *
 * @UniqueEntity(
 *     fields={"code"},
 *     message="This code is already in use."
 * )
 * @ImportAssert\Product
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(name="intProductDataId", type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="strProductCode", type="string", length=10, unique=true)
     *
     * @Assert\NotBlank(message="Code should not be blank.")
     */
    protected $code;

    /**
     * @ORM\Column(name="strProductName", type="string", length=50)
     *
     * @Assert\NotBlank(message="Name should not be blank.")
     * @Assert\Length(max=50)
     */
    protected $name;

    /**
     * @ORM\Column(name="strProductDesc", type="string", length=255)
     *
     * @Assert\NotBlank(message="Description should not be blank.")
     * @Assert\Length(max=255)
     */
    protected $description;

    /**
     * @ORM\Column(name="intStock", type="integer", options={"unsigned"=true})
     * 
     * @Assert\NotBlank(message="Stock should not be blank.")
     * @Assert\Type(type="integer", message="Stock should contain only numbers")
     * @Assert\Range(min=0, minMessage="Cost should must be more than 0")
     */
    protected $stock;

    /**
     * @ORM\Column(name="floatCost", type="float")
     * 
     * @Assert\NotBlank(message="Cost should not be blank.")
     * @Assert\Type(type="numeric", message="Cost should contain only numbers")
     * @Assert\Range(min=0, minMessage="Cost should must be more than 0")
     */
    protected $cost;

    /**
     * @ORM\Column(name="dtmAdded", type="datetime", nullable=true)
     */
    protected $added;

    /**
     * @ORM\Column(name="dtmDiscontinued", type="datetime", nullable=true)
     */
    protected $discontinued;

    /**
     * @ORM\Column(name="stmTimestamp", type="datetime")
     */
    protected $timestamp = 'CURRENT_TIMESTAMP';

    /**
     * Product constructor.
     */
    public function __construct()
    {
        # Default values for datetime and timestamp columns
        $this->added        = new DateTime();
        $this->timestamp    = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return Product
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set cost
     *
     * @param float $cost
     *
     * @return Product
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set added
     *
     * @param \DateTime $added
     *
     * @return Product
     */
    public function setAdded($added)
    {
        $this->added = $added;

        return $this;
    }

    /**
     * Get added
     *
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Set discontinued
     *
     * @param \DateTime $discontinued
     *
     * @return Product
     */
    public function setDiscontinued($discontinued)
    {
        $this->discontinued = $discontinued;

        return $this;
    }

    /**
     * Get discontinued
     *
     * @return \DateTime
     */
    public function getDiscontinued()
    {
        return $this->discontinued;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     *
     * @return Product
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }
}
