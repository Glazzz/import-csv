<?php
use Itransition\ImportCsvBundle\Command\ImportCsvCommand;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImportCommandTest extends KernelTestCase
{
    /**
     * Run test command without params
     */
    public function testWithoutParams()
    {
        $this->runCommand();
    }

    /**
     * Run test command with wrong file extension
     */
    public function testWrongFileExtension()
    {
        $this->runCommand(['--file_path' => 'web/import/stock.xls'], 'Not valid file extension.');
    }

    /**
     * Run test command with wrong file path
     */
    public function testFilePathParam()
    {
        $this->runCommand(['--file_path' => 'web/import/wrong-path'], 'Not valid file extension');
    }

    /**
     * Run test with skipped products
     */
    public function testProductsSkipped()
    {
        $this->runCommand([], 'Products skipped:');
    }

    /**
     * Run command
     * @param array $params
     * @param string $assertContains
     */
    private function runCommand(array $params = array(), $assertContains = 'Command successfully completed')
    {
        # Get kernel
        $kernel = $this->createKernel();
        $kernel->boot();

        # New application
        $application = new Application( $kernel );
        $application->add( new ImportCsvCommand() );

        # Get command
        $command = $application->find('import:csv');

        # Generate command params
        $params = array_merge( array(
            'command' => $command->getName()
        ), $params );

        # Run command test
        try {
            $commandTester = new CommandTester( $command );
            $commandTester->execute( $params );

            # Assert Contains
            $this->assertContains($assertContains, $commandTester->getDisplay());

        } catch ( \Exception $e ) {
            $this->assertContains( $assertContains, $e->getMessage() );
        }
    }
}