<?php

namespace Itransition\ImportCsvBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;

class ImportCsvCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('import:csv')
            ->setDescription('Import csv command')
            ->addArgument('test', InputArgument::OPTIONAL, 'Activate test mode', false)
            ->addOption('file_path', null, InputOption::VALUE_OPTIONAL, 'Custom path to .csv file', realpath('web/import') . '/stock.csv')
            ->addOption('truncate', null, InputOption::VALUE_OPTIONAL, 'Enable or disable truncate before writer process', false);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        # Rule: Using a command line argument the script can be run in 'test' mode.
        $isTestMode = $input->getArgument( 'test' ) === 'test';

        # Set truncate
        $shouldTruncate = $input->getOption( 'truncate' ) === 'yes' || $input->getOption( 'truncate' ) == 1;

        # File path from option value
        $filePath = $input->getOption('file_path');

        # Open file
        $csvFile = new \SplFileObject( $filePath );

        # Csv read
        $csvReader = new CsvReader( $csvFile );

        # Import products
        $importHandler = $this->getContainer()->get('itransition.import');
        $importHandler->run( $csvReader, $isTestMode, $shouldTruncate );

        # Print import information
        $output->writeln( '<info>Import information:</info>' );
        $output->writeln( 'Total products for import: ' . ( $importHandler->getSuccessfulCount() + $importHandler->getSkippedCount() ) );
        $output->writeln( 'Successful import products count:' . $importHandler->getSuccessfulCount() );
        $output->writeln( 'Skipped import products count:' . $importHandler->getSkippedCount() );

        # Print error log
        if ( $importHandler->getSkippedCount() > 0 ) {
            $output->writeln( '<error>Products skipped:</error>' );
            foreach ( $importHandler->getErrorLog() as $skippedProduct ) {
                $output->writeln( $skippedProduct );
            }
        }

        $output->writeln( 'Command successfully completed' );
    }
}
