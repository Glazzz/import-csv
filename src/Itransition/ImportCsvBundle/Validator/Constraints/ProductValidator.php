<?php
namespace Itransition\ImportCsvBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProductValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
        # Get values
        $cost   = $protocol->getCost();
        $stock  = $protocol->getStock();

        # Get container
        global $kernel;
        if ( empty( $kernel ) ) {
            $kernel = new \AppKernel('test', false);
            $kernel->boot();
        }
        $container = $kernel->getContainer();

        # Get parameters
        $minCost    = $container->getParameter('product_validator.min_cost');
        $maxStock   = $container->getParameter('product_validator.max_stock');
        $maxCost    = $container->getParameter('product_validator.max_cost');

        # Rule: Any stock items which cost over $1000 will not be imported.
        if ( (float) $cost > $maxCost ) {
            $this->context->buildViolation( sprintf('Any stock items which cost over $1000 will not be imported. Current cost value: %f', $cost) )
                ->addViolation();
        }

        # Rule: Any stock item which costs less that $5 and has less than 10 stock will not be imported.
        if ( (int) $stock < $maxStock && (int) $cost < $minCost ) {
            $this->context->buildViolation( sprintf('Any stock item which costs less that $5 and has less than 10 stock will not be imported. Current stock value: %d. Current cost value: %f', $stock, $cost) )
                ->addViolation();
        }
    }
}