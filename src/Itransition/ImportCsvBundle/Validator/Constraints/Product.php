<?php
namespace Itransition\ImportCsvBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Product extends Constraint
{
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}