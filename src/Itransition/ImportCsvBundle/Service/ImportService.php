<?php

namespace Itransition\ImportCsvBundle\Service;

use Symfony\Component\Validator\Validator\RecursiveValidator;
use Itransition\ImportCsvBundle\Helpers\Product as Product_Helper;
use Itransition\ImportCsvBundle\Entity\Product;
use Itransition\ImportCsvBundle\Helpers\DoctrineWriter;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Workflow\StepAggregator as Workflow;
use Ddeboer\DataImport\Step\FilterStep;
use Doctrine\ORM\EntityManager;
use Ddeboer\DataImport\Step\ValueConverterStep;
use DateTime;
use Exception;

class ImportService
{
    /**
     * Successful counter
     * @var int
     */
    private $successfulCount = 0;

    /**
     * Skipped counter
     * @var int
     */
    private $skippedCount = 0;

    /**
     * Failed products log
     * @var array
     */
    private $errorLog = [];

    /**
     * Manager data
     * @var EntityManager
     */
    private $manager;

    /**
     * Products codes for check unique code
     * @var array
     */
    private $productsCodes = [];

    /**
     * Data validator
     * @var RecursiveValidator
     */
    private $validator;

    /**
     * Product instance
     * @var \Itransition\ImportCsvBundle\Entity\Product
     */
    private $productInstance;
    
    public function __construct( EntityManager $manager, RecursiveValidator $validator )
    {
        $this->manager = $manager;
        $this->validator = $validator;
    }

    /**
     * Get successful counter
     * @return int
     */
    public function getSuccessfulCount()
    {
        return $this->successfulCount;
    }

    /**
     * Get skipped counter
     * @return int
     */
    public function getSkippedCount()
    {
        return $this->skippedCount;
    }

    /**
     * Error log data
     * @return array
     */
    public function getErrorLog()
    {
        return $this->errorLog;
    }

    /**
     * Import from products data array
     * @param CsvReader $csvReader
     * @param bool $isTestMode
     * @param bool $truncate
     * @return bool
     * @throws Exception
     */
    public function run( CsvReader $csvReader, $isTestMode = false, $truncate = false)
    {
        # Reset successful and skipped counters
        $this->resetCounter();

        $productHelper = new Product_Helper( $this->manager );

        # Set data fields keys
        $csvReader->setHeaderRowNumber( 0 );
        $headerColumns = $productHelper->getFieldsListByAliases( $csvReader->getColumnHeaders() );
        $csvReader->setColumnHeaders( $headerColumns );

        # Check is valid file
        if ( ! $csvReader->valid() ) {
            throw new Exception('Csv file is not valid.');
        }
        
        # Check is not empty data
        if ( ! $total = $csvReader->count() ) {
            throw new Exception('Csv file is empty.');
        }

        # Errors products
        if ( $csvReader->hasErrors() ) {
            foreach ( $csvReader->getErrors() as $productError ) {
                $productDataError = [];
                foreach ( $headerColumns as $key => $column ) {
                    $productDataError[ $column ] = isset( $productError[ $key ] ) ? $productError[ $key ] : '';
                }
                $this->addFailedProduct( $productDataError, 'Not valid record' );
            }
        }

        # Create the workflow from the reader
        $workflow = new Workflow($csvReader);

        # Create a writer: you need Doctrine’s EntityManager.
        if ( !$isTestMode ) {
            # New writer instance
            $doctrineWriter = new DoctrineWriter($this->manager, 'Itransition\ImportCsvBundle\Entity\Product');

            # Disable truncate before writer process
            $doctrineWriter->setTruncate( $truncate );

            # Add writer to workflow
            $workflow->addWriter($doctrineWriter);
        }

        # Add filters
        $filterStep = $this->addRecordsFilters();

        # Add filters to steps
        $workflow->addStep($filterStep);


        # Rule: Any stock item marked as discontinued will be imported, but will have the discontinued date set as the current date.
        $valueStep = new ValueConverterStep();
        $valueStep->add('[discontinued]', function($discontinued) {

            if ( $discontinued === 'yes' ) {
                return new DateTime();
            }
            return null;
        });
        $workflow->addStep($valueStep);

        # Process the workflow
        $workflow->process();

        # Statistics
        $this->successfulCount = $total - $this->skippedCount;
        
        return true;
    }

    /**
     * Products filters
     * @return $this
     */
    protected function addRecordsFilters()
    {
        $filterStep = (new FilterStep())
            ->add([$this, 'checkProductValidateByData'])
            ->add([$this, 'filterProductCode']);

        return $filterStep;
    }

    /**
     * @param string $code
     * @return bool
     */
    public function isExistProductCode($code)
    {
        return isset( $this->productsCodes[$code ] );
    }

    /**
     * @param string $code
     */
    public function addProductCode($code)
    {
        $this->productsCodes[ $code ] = true;
    }

    /**
     * Filter for check unique product code
     * @param array $productData
     * @return bool
     */
    public function filterProductCode(array $productData)
    {
        if ( $this->isExistProductCode( $productData['code'] ) ) {
            $this->addFailedProduct($productData, 'This code is already in use.');
            return false;
        }

        $this->addProductCode( $productData['code'] );

        return true;
    }

    /**
     * @param array $productData
     * @return bool
     */
    public function checkProductValidateByData(array $productData)
    {
        # Add new product
        $product = $this->getProductInstance();
        $product->setCode( $productData['code'] );
        $product->setName( $productData['name'] );
        $product->setDescription( $productData['description'] );
        $product->setStock( $productData['stock'] );
        $product->setCost( $productData['cost'] );

        # Check is valid product
        $errors = $this->validator->validate($product);

        # if is not valid, then increments skipped counter and write product to error log
        if ( $errors->count() ) {
            $this->addFailedProduct( $productData, $errors[0]->getMessage() );
            return false;
        }

        return true;
    }

    /**
     * Get product instance
     * @return Product
     */
    private function getProductInstance()
    {
        if ( $this->productInstance === null ) {
            $this->productInstance = new Product();
        }
        return $this->productInstance;
    }

    /**
     * Change skipped counter and add record to error log
     * @param array $productData
     * @param string $message
     */
    private function addFailedProduct(array $productData, $message = '')
    {
        $this->skippedCount++;
        $this->errorLog[] = sprintf('Product code: %s. Product Name: %s. Message: %s', $productData['code'], $productData['name'], $message);
    }

    /**
     * Reset successful and skipped counters
     */
    private function resetCounter()
    {
        $this->successfulCount  = 0;
        $this->skippedCount     = 0;
        $this->errorLog         = [];
        $this->productsCodes    = [];
    }
}