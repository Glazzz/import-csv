<?php
namespace Itransition\ImportCsvBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class);
        $builder->add('code', TextType::class);
        $builder->add('description', TextareaType::class);
        $builder->add('stock', IntegerType::class);
        $builder->add('cost', NumberType::class);
        $builder->add('discontinued', CheckboxType::class, ['required' => false]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}